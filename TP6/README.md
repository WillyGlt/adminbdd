## docker-compose.yml
```yml
version: '3.7'

services:
    master:
        image: mariadb:10.4
        restart: on-failure
        environment:
            MYSQL_ROOT_PASSWORD: password
        volumes:
            - ./master:/var/lib/mysql
            - ./backups:/backups
            - ./config/master.cnf:/etc/mysql/mariadb.conf.d/master.cnf
            - ./scripts:/scripts
        networks:
            - internal
    slave:
        image: mariadb:10.4
        restart: on-failure
        environment:
            MYSQL_ROOT_PASSWORD: password
        volumes:
            - ./slave:/var/lib/mysql
            - ./backups:/backups
            - ./config/slave.cnf:/etc/mysql/mariadb.conf.d/slave.cnf
            - ./scripts:/scripts
        depends_on:
            - master
        networks:
            - internal
networks:
    internal:
```

## Fichier de configurations Master et Slave
Dans le dossier /etc/mysql/mariadb.conf.d/

master.cnf
```
[mariadb]
log-bin
server_id=1
log-basename=master1
binlog-format=mixed
```
slave.cnf
```
[mariadb]
log-bin
server_id=1
log-basename=slave1
binlog-format=mixed
```

## Script ajout user avec droits de replication Master
```sql
CREATE USER IF NOT EXISTS 'replicant'@'%' identified by 'replicant_password';

grant replication slave on *.* to replicant;

flush privileges;
```
`/scripts#mysql -u root --password==password < replicant.sql`

## Démarrer le serveur Master
Serveur démarrer automatiquement.

## Ajouter le Master au Slave
```
CHANGE MASTER TO
MASTER_HOST='maria',
MASTER_USER='replicant',
MASTER_PASSWORD='replicant_password',
MASTER_PORT=3306,
MASTER_LOG_FILE='master1-bin.000007',
MASTER_LOG_POS=344,
MASTER_CONNECT_RETRY=10;
```

## Démarrer et vérifier l'état du Slave
```sql
START SLAVE;
SHOW SLAVE STATUS;
```

## Créer une nouvelle base de données et table sur le serveur Master
```sql
DROP DATABASE if exists db_test;
CREATE DATABASE db_test;
USE db_test;
CREATE TABLE clients ( nom VARCHAR(255), prenom VARCHAR(255), date DATE, cp INT);
INSERT INTO clients (nom, prenom, cp) VALUES ("noris", "chuck", 64200);
```