## docker-compose1.yml
```yml
version: '3.7'

services:
  mariadb1:
    image: mariadb:10.4
    restart: on-failure
    command: mysqld --wsrep-new-cluster
    environment:
        MYSQL_ROOT_PASSWORD: password
    volumes:
        - ./mariadb1:/var/lib/mysql
        - ./config/mariadb1.cnf:/etc/mysql/mariadb.conf.d/mariadb1.cnf
    expose:
      - 3306
      - 4444
      - 4567
      - 4568
    networks:
      - internal

networks:
    internal:
```

## docker-compose2.yml
```yml
version: '3.7'

services:
  mariadb2:
    image: mariadb:10.4
    restart: on-failure
    environment:
        MYSQL_ROOT_PASSWORD: password
    volumes:
        - ./mariadb2:/var/lib/mysql
        - ./config/mariadb2.cnf:/etc/mysql/mariadb.conf.d/mariadb2.cnf
    expose:
      - 3306
      - 4444
      - 4567
      - 4568
    depends_on:
      - mariadb1
    networks:
      - internal

networks:
    internal:
```

## docker-compose3.yml
```yml
version: '3.7'

services:
  mariadb3:
    image: mariadb:10.4
    restart: on-failure
    environment:
        MYSQL_ROOT_PASSWORD: password
    volumes:
        - ./mariadb3:/var/lib/mysql
        - ./config/mariadb3.cnf:/etc/mysql/mariadb.conf.d/mariadb3.cnf
    expose:
      - 3306
      - 4444
      - 4567
      - 4568
    depends_on:
      - mariadb1
    networks:
      - internal

networks:
    internal:
```

## mariadb1.cnf
```
[mariadb]
wsrep_provider=/usr/lib/libgalera_smm.so
wsrep_cluster_address="gcomm://mariadb1,mariadb2,mariadb3"
wsrep_node_address="mariadb1"
wsrep_node_name="mariadb1"
binlog_format=ROW
default_storage_engine=InnoDB
innodb_autoinc_lock_mode=2
innodb_doublewrite=1
query_cache_size=0
wsrep_on=ON
```

## mariadb2.cnf
```
[mariadb]
wsrep_provider=/usr/lib/libgalera_smm.so
wsrep_cluster_address="gcomm://mariadb1,mariadb2,mariadb3"
wsrep_node_address="mariadb2"
wsrep_node_name="mariadb2"
binlog_format=ROW
default_storage_engine=InnoDB
innodb_autoinc_lock_mode=2
innodb_doublewrite=1
query_cache_size=0
wsrep_on=ON
```

## mariadb3.cnf
```
[mariadb]
wsrep_provider=/usr/lib/libgalera_smm.so
wsrep_cluster_address="gcomm://mariadb1,mariadb2,mariadb3"
wsrep_node_address="mariadb3"
wsrep_node_name="mariadb3"
binlog_format=ROW
default_storage_engine=InnoDB
innodb_autoinc_lock_mode=2
innodb_doublewrite=1
query_cache_size=0
wsrep_on=ON
```

## Redémarrez les nodes et vérifiez le bon fonctionnement du cluster
`command: mysqld --wsrep-new-cluster`

Cette commande présente dans notre premier docker-compose permet de garder un cluster fonctionnel après redémarrage