CREATE DATABASE db_test;
USE db_test;
CREATE TABLE clients ( nom VARCHAR(255), prenom VARCHAR(255), date DATE, cp INT);
INSERT INTO clients (nom, prenom, cp) VALUES ("noris", "chuck", 64200);
SELECT prenom, cp FROM clients;
