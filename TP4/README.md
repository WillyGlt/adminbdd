## Dockerfile
```
FROM mysql

RUN apt-get update && apt-get -y install vim cron logrotate
```

## docker-compose.yml
```yml
version: '3.7'

services:
  custom-mysql:
    build: .
    restart: on-failure
    environment:
      MYSQL_ROOT_PASSWORD: password
    volumes:
        - ./mysql:/var/lib/mysql
        - ./backups:/backups
```

## Crontab
```
* 17 * * 1 root mysqldump -u root --password=password --all-databases | gzip -9 -c > /backups/all_databases`date +"\%Y-\%m-\%d_\%H:\%M:\%S"`.sql.gz
```

## Lancer le service Cron
```
service cron start
```

## Logrotate
```
/backups/all_databases.sql {
    rotate 5
    daily
    compresscmd /usr/bin/bzip2
    compressext .bz2
    compress
    missingok
    prerotate
        mysqldump -u root --password=${MYSQL_ROOT_PASSWORD} --all-databases > /backups/all_databases.sql
    endscript
}
```