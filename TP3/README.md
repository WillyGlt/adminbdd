```sql
DROP DATABASE if exists teams;
DROP USER if exists 'manager'@'localhost';
DROP USER if exists 'recruiter'@'localhost';
CREATE DATABASE teams;
USE teams;
CREATE TABLE games (match_date DATE, victory BIT, observations TINYTEXT);
-- BIT pcq comme Booléen avec le 0 et le 1 et TINYTEXT pcq observations courtes
CREATE TABLE players (firstname VARCHAR(255), lastname VARCHAR(255), start_sate DATE);
GRANT ALL PRIVILEGES ON games TO 'manager'@'localhost' IDENTIFIED BY 'manager_password';
GRANT INSERT, SELECT, UPDATE, DROP, CREATE, DELETE ON players TO 'recruiter'@'localhost' IDENTIFIED BY 'recruiter_password';
FLUSH PRIVILEGES;
EXIT
mysql -u manager -p
manager_password
USE teams;
INSERT INTO games (match_date, victory, observations) VALUES ("2020-09-26", (1), "une belle victoire");
INSERT INTO games (match_date, victory, observations) VALUES ("2020-09-27", (0), "un manque de cohesion qui entraina la defaite inevitable");
INSERT INTO games (match_date, victory, observations) VALUES ("2020-09-28", (1), "Un match difficile mais une victoire à la clef");
EXIT
mysql -u root -p
password
```