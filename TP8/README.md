## docker-compose.yml
```yml
version: '3.7'

services:
  mariadb:
    image: mariadb
    restart: on-failure
    environment:
      MYSQL_ROOT_PASSWORD: password
    volumes:
        - ./mariadb:/var/lib/mysql
    expose:
      - 3306
    ports:
      - "3306:3306"
    networks:
     - internal

  exporter:
    image: prom/mysqld-exporter
    restart: on-failure
    environment:
      DATA_SOURCE_NAME: "root:password@(mariadb:3306)/"
    ports:
      - "9104:9104"
    expose:
      - 9104
    depends_on:
      - mariadb
    networks:
      - internal

  prometheus:
    image: prom/prometheus
    restart: on-failure
    volumes:
      - ./prometheus.yml:/etc/prometheus/prometheus.yml
    ports:
      - "9090:9090"
    depends_on:
      - exporter
    networks:
      - internal

networks:
  internal:
```

## prometheus.yml
```yml
global:
    scrape_interval:     15s
    evaluation_interval: 15s

alerting:
    alertmanagers:
    - static_configs:
      - targets:
        # - alertmanager:9093
  
rule_files:
    # - "first_rules.yml"
    # - "second_rules.yml"

scrape_configs:
    - job_name: 'prometheus'

      static_configs:
        - targets: ['localhost:9090']

    - job_name: 'mariadb'
    
      static_configs:
        - targets: ['exporter:9104']
```

## Création d'un graphique qui affiche toutes les opérations de lectures et d'écritures
`mysql_global_status_commands-total{command="select"} OR mysql_global_status_commands-total{command="delete"}`

## Création d'un graphique qui affiche la variation du taux d'opérations de lectures et d'écritures en prenant en compte la moyenne sur les 5 dernières minutes
`sum(rate(mysql_global_status_commands-total{command="select"}[5m]) OR rate(mysql_global_status_commands-total{command="delete"}[5m]))`

