## docker-compose.yml
```yml
version: '3.7'

services:
  mysql:
  image: mysql
  restart: on-failure
  environment:
    MYSQL_ROOT_PASSWORD: password
  volumes:
    - ./mysql:/var/lib/mysql
    - ./backups:/backups

  maria:
    images: maria
    restart: on-failure
    environment:
        MYSQL_ROOT_PASSWORD: password
  volumes:
    - ./maria:/var/lib/mysql
    - ./backups:/backups
```

## Créer une base de données MYSQL
```sql
DROP DATABASE if exists db_test;
CREATE DATABASE db_test;
USE db_test;
CREATE TABLE clients ( nom VARCHAR(255), prenom VARCHAR(255), date DATE, cp INT);
INSERT INTO clients (nom, prenom, cp) VALUES ("noris", "chuck", 64200);
```

## Exporter la table db_test dans le dossier /backups
`mysqldump -u root --password=password db_test > backups/db_test.sql`

##  Connexion au serveur MariaDB
`mariadb -u root --password=password`

## Importer la base exportée
On créé tout d'abord la base db_test:
```sql
CREATE DATABASE db_test;
```
Puis on l'importe:
`mariadb -u root --password=password db_test < backups/db_test.sql`