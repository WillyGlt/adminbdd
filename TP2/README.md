```sql
CREATE DATABASE events;
USE events;
CREATE TABLE public_events ( event_date DATE, event_name VARCHAR(255), event_age_requirement INT);
CREATE TABLE private_events AS SELECT * FROM public_events;
GRANT ALL PRIVILEGES ON *.* TO 'event_manager'@'localhost' IDENTIFIED BY 'password';
GRANT SELECT ON public_events TO 'event_supervisor'@'localhost' IDENTIFIED BY 'password';
FLUSH PRIVILEGES;
EXIT
mysql -u event_manager  -p
password
USE events;
INSERT INTO public_events (event_date, event_name, event_age_requirement) VALUES ("2020-09-28", "AdminBDD", 18);
INSERT INTO public_events (event_date, event_name, event_age_requirement) VALUES ("2020-09-29", "Infra", 18);
INSERT INTO private_events (event_date, event_name, event_age_requirement) VALUES ("2020-09-28", "McDo", 10);
INSERT INTO private_events (event_date, event_name, event_age_requirement) VALUES ("2020-09-29", "Entrecote", 20);
EXIT
mysql -u event_supervisor -p
password
USE events;
SELECT * FROM public_events;
SELECT * FROM private_events;
ERROR 1142 (42000): SELECT command denied to user 'event_supervisor'@'localhost' for table 'private_events'
EXIT
mysql -u event_supervisor -p
password
DROP USER 'event_supervisor'@'localhost';
```